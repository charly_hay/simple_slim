module SimpleSlim

  #
  # Represents a SEPA Direct Debit Issue
  #
  class DirectDebitIssue

    attr_accessor :id, :reject_amount, :reject_reason, :currency, :type, :date,
      :return_reason_code, :date_created, :execution_status, :direct_debit_id

    def initialize
      yield(self) if block_given?
    end

    def self.search
      params = {
        scheme: 'SEPA.DIRECT_DEBIT.CORE',
        creditorReference: Slimpay.configuration.creditor_reference,
        executionStatus: 'toprocess'
      }

      payment_issues = Slimpay::PaymentIssue.new.search(params)
      return unless payment_issues['page']['totalElements'] > 0

      payment_issues['_embedded']['paymentIssues'].map do |ddi|
        build_from_hash(ddi)
      end
    end

    def acknowledge
      Slimpay::PaymentIssue.new.acknowledge(id)['executionStatus'] == 'processed'
    end

    def get_direct_debit
      @direct_debit ||= DirectDebit.get_from_id(direct_debit_id)
    end

    protected

    #
    # Builds a +SimpleSlim::DirectDebitIssue+ instance from a Slimpay representation
    #
    # @param [Hash]
    # @return [SimpleSlim::DirectDebitIssue]
    #
    def self.build_from_hash(hsh)
      SimpleSlim::DirectDebitIssue.new do |ddi|
        ddi.id                 = hsh['id']
        ddi.reject_amount      = hsh['rejectAmount']
        ddi.reject_reason      = hsh['rejectReason']
        ddi.currency           = hsh['currency']
        ddi.type               = hsh['type']
        ddi.return_reason_code = hsh['returnReasonCode']
        ddi.date               = hsh['dateCreated'].to_datetime
        ddi.execution_status   = hsh['executionStatus']
        ddi.direct_debit_id    = get_direct_debit_id(hsh)
      end
    end

    def self.get_direct_debit_id(hsh)
      hsh['_links']["https://api.slimpay.net/alps#get-payment"]["href"].split("/").last
    end
  end
end
