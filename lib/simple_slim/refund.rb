module SimpleSlim

  #
  # Represents a SEPA Direct Debit
  #
  class Refund

    attr_accessor :subscriber, :amount

    def initialize
      yield(self) if block_given?
    end

    #
    # Creates a Refund for a specific subscriber
    #
    # @param subscriber [String] Subscriber reference
    # @param amount [Fixnum] Charged amount in EUR cents
    # @param label [String]
    # @return [SimpleSlim::DirectDebit]
    #
    def self.create(subscriber, amount, label = nil)
      refund = Slimpay::Base.new

      res = refund.create_payouts({
        creditor:         { reference: Slimpay.configuration.creditor_reference },
        subscriber:       { reference: subscriber },
        scheme:           'SEPA.CREDIT_TRANSFER',
        amount:           amount.to_f / 100,
        currency:         'EUR',
        label:            label
      })

      build_from_hash(JSON.load(res.parsed_response))
    end

    protected

    #
    # Builds a +SimpleSlim::Refund+ instance from a Slimpay representation
    #
    # @param [Hash]
    # @return [SimpleSlim::Refund]
    #
    def self.build_from_hash(hsh)
      SimpleSlim::Refund.new do |dd|
        dd.id               = hsh['id']
        dd.amount           = BigDecimal(hsh['amount'])
        dd.reference        = hsh['paymentReference']
        dd.label            = hsh['label']
        dd.execution_status = hsh['executionStatus']
        dd.sequence_type    = hsh['sequenceType']
        dd.currency         = hsh['currency']
        dd.date             = (hsh['dateValued'] || hsh['executionDate']).try(:to_datetime)
      end
    end

  end
end
